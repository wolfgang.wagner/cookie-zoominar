# Cookie Consent Manager ohne Extension

TYPO3-Installation für das Zoominar "Cookie Consent Manager für TYPO3 ohne Extension"

Direkt verwendbar mit Docker+DDEV.

1. Klone dieses Repository (oder download als zip)
2. ddev start
3. ddev composer install
4. ddev import-db < database.sql
5. ddev launch /typo3

Benutzername: admin
Passwort: password

Das Sitepackage mit den relevanten Dateien findest Du im Verzeichnis Source/Extensions/vt9/

