mod.wizards.newContentElement.wizardItems.common {
    elements {
            vt9card {
                iconIdentifier = tx_vt9maskelements_vt9card
                title = LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.vt9card_title
                description = LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.vt9card_description
                tt_content_defValues {
                    CType = vt9maskelements_vt9card
                }
            }
            vt9slider {
                iconIdentifier = tx_vt9maskelements_vt9slider
                title = LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.vt9slider_title
                description = LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.vt9slider_description
                tt_content_defValues {
                    CType = vt9maskelements_vt9slider
                }
            }
    }
    show := addToList(vt9card, vt9slider)
}
